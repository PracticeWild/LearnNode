const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const reviewSchema = new mongoose.Schema({
    date: {
        type: Date,
        default: Date.now
    },
    author: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: 'There must be an User'
    },
    store: {
        type: mongoose.Schema.ObjectId,
        ref: 'Store',
        required: 'There must be a Store'
    },
    text: {
        type: String,

    },
});

module.exports = mongoose.model('Review', reviewSchema);